#!/bin/sh

docker pull registry.gitlab.com/xobtah/fetish-rs:${TAG}
docker stop fetish && docker rm fetish
docker-compose up -d fetish
